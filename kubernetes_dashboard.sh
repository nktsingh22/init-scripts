#!/bin/bash
action=$1
if [[ "$action" == "create" ]]; then
echo "Installing Kubernetes Dashboard and supporting components"
#Create a self-signed cert to work with ALB on HTTPS
openssl genrsa 2048 > my-private-key.pem
openssl req -new -x509 -nodes -sha256 -days 365 -key my-private-key.pem -outform PEM -out my-certificate.pem -subj "/C=IN/O=CTS/OU=CIS/CN=*.dashbaord.com"
cert_arn=$(aws acm import-certificate --certificate fileb://my-certificate.pem --private-key fileb://my-private-key.pem | jq -r .CertificateArn)
#cert_arn=$(aws acm list-certificates | jq -rc .CertificateSummaryList[].CertificateArn)
#Install Kubernetes Dashboard
helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
helm upgrade --install kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard --create-namespace --namespace kubernetes-dashboard

#Requires AWS Load Balancer controller installation
#Expose via Ingress Load Balancer

cat > dashboard-ingress.yaml <<EOF
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  namespace: kubernetes-dashboard
  name: dashboard-ingress
  annotations:
    alb.ingress.kubernetes.io/scheme: internet-facing
    alb.ingress.kubernetes.io/target-type: ip
    alb.ingress.kubernetes.io/backend-protocol: HTTPS
    alb.ingress.kubernetes.io/certificate-arn: $cert_arn
    alb.ingress.kubernetes.io/listen-ports: '[{"HTTP": 80}, {"HTTPS":443}]'
    alb.ingress.kubernetes.io/ssl-redirect: '443' 
spec:
  ingressClassName: alb
  rules:
    - http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: kubernetes-dashboard
                port:
                  number: 443
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
---
apiVersion: v1
kind: Secret
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
  annotations:
    kubernetes.io/service-account.name: "admin-user"
type: kubernetes.io/service-account-token
EOF
cat dashboard-ingress.yaml
kubectl apply -f dashboard-ingress.yaml
echo "#######################################"
echo Dashboard Url is : https://$(kubectl get ing dashboard-ingress -n kubernetes-dashboard -o=jsonpath='{.status.loadBalancer.ingress[0].hostname}')
echo "Password is: $(kubectl get secret admin-user -n kubernetes-dashboard -o jsonpath={".data.token"} | base64 -d)"

elif [[ "$action" == "delete" ]]; then
#Delete
    echo "Deleting Kubernetes Dashboard Components"
    kubectl delete -f dashboard-ingress.yaml
    helm delete kubernetes-dashboard --namespace kubernetes-dashboard
    sleep 30
    aws acm delete-certificate --certificate-arn $(aws acm list-certificates | jq -rc .CertificateSummaryList[].CertificateArn)
else
    echo "Please provide arguments: 'create' or 'delete'"
fi

#curl https://gitlab.com/nktsingh22/init-scripts/-/raw/main/kubernetes_dashboard.sh | bash -s -- create
#curl https://gitlab.com/nktsingh22/init-scripts/-/raw/main/kubernetes_dashboard.sh | bash -s -- delete