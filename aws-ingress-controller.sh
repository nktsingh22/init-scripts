#!/bin/bash
action=$1
if [[ "$action" == "create" ]]; then
    echo "Installing AWS Load Balancer Controller"
    clusterName="spot-cluster-Ankit"
    helm repo add eks https://aws.github.io/eks-charts
    helm repo update eks
    helm install aws-load-balancer-controller eks/aws-load-balancer-controller \
      --namespace kube-system \
      --set clusterName=${clusterName} \
      --set serviceAccount.create=false \
      --set serviceAccount.name=aws-load-balancer-controller
    kubectl get deployment -n kube-system aws-load-balancer-controller

elif [[ "$action" == "delete" ]]; then
#Delete
    echo "Deleting AWS Load Balancer Controller"
    helm uninstall aws-load-balancer-controller --namespace kube-system
else
    echo "Please provide arguments: 'create' or 'delete'"
fi

#curl https://gitlab.com/nktsingh22/init-scripts/-/raw/main/aws-ingress-controller.sh | bash -s -- create
#curl https://gitlab.com/nktsingh22/init-scripts/-/raw/main/aws-ingress-controller.sh | bash -s -- delete