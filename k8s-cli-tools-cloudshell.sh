#!/bin/bash
#EKS-CTL
echo "Installing eks-ctl"
curl -s --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin
echo "Installed"
#K9s
echo "Installing k9s"
curl -sS https://webinstall.dev/k9s | bash
source ~/.config/envman/PATH.env
echo "Installed"
#Helm
echo "Installing helm"
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
sh get_helm.sh
rm -fr get_helm.sh
echo "Installed"
#Validate
echo "#############################"
aws --version
echo "#############################"
echo "eksctl: $(eksctl version)"
echo "#############################"
echo "kubectl:"
kubectl version --client
echo "#############################"
k9s version
echo "#############################"
echo "Helm: $(helm version)"
