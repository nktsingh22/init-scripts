#!/bin/bash
action=$1
if [[ "$action" == "create" ]]; then
    echo "Installing Cluster Autoscaler"
    clusterName="spot-cluster-Ankit"
    curl https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml | sed "s/<YOUR CLUSTER NAME>/${clusterName}" | kubectl apply -f -

elif [[ "$action" == "delete" ]]; then
#Delete
    echo "Deleting Cluster Autoscaler"
    
else
    echo "Please provide arguments: 'create' or 'delete'"
fi

#curl https://gitlab.com/nktsingh22/init-scripts/-/raw/main/cluster-autoscaler.sh | bash -s -- create
#curl https://gitlab.com/nktsingh22/init-scripts/-/raw/main/cluster-autoscaler.sh | bash -s -- delete