#!/bin/bash
#AWS-CLI
echo "Installing aws-cli"
apk add aws-cli > /dev/null
echo "Installed"
#EKS-CTL
echo "Installing eks-ctl"
curl -s --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin
echo "Installed"
#Kubectl
echo "Installing kubectl"
curl -sLO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
rm -fr kubectl
echo "Installed"
#K9s
echo "Installing k9s"
curl -sS https://webinstall.dev/k9s | bash > /dev/null
source ~/.config/envman/PATH.env
echo "Installed"
#K9s
echo "Installing helm"
apk add helm > /dev/null
echo "Installed"
#Validate
echo "#############################"
aws --version
echo "#############################"
echo "eksctl: $(eksctl version)"
echo "#############################"
echo "kubectl:"
kubectl version --client
echo "#############################"
k9s version
echo "#############################"
helm version
