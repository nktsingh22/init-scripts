#!/bin/bash
action=$1
if [[ "$action" == "create" ]]; then
    echo "Creating Cluster.."
    curl -sO https://gitlab.com/nktsingh22/init-scripts/-/raw/main/eksctl-cluster.yaml
    eksctl create cluster -f eksctl-cluster.yaml
elif [[ "$action" == "delete" ]]; then
#Delete
    echo "Deleting any Ingress Load Balancers"
    kubectl delete ing --all --all-namespaces
    echo "Deleting Cluster.."
    curl -sO https://gitlab.com/nktsingh22/init-scripts/-/raw/main/eksctl-cluster.yaml
    eksctl delete cluster -f eksctl-cluster.yaml
else
    echo "Please provide arguments: 'create' or 'delete'"
fi

#curl https://gitlab.com/nktsingh22/init-scripts/-/raw/main/eksctl-cluster.sh | bash -s -- create
#curl https://gitlab.com/nktsingh22/init-scripts/-/raw/main/eksctl-cluster.sh | bash -s -- delete