#!/bin/bash
action=$1
if [[ "$action" == "create" ]]; then
    echo "Installing Nginx Ingress Controller"
    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
    helm repo update
    helm upgrade -i ingress-nginx ingress-nginx/ingress-nginx \
        --namespace kube-system \
        --set controller.service.type=LoadBalancer \
        --set controller.service.annotations."service\.beta\.kubernetes\.io/aws-load-balancer-type"="nlb"

    kubectl get deployment -n kube-system ingress-nginx-controller
elif [[ "$action" == "delete" ]]; then
#Delete
    echo "Deleting Nginx Ingress Controller"
    helm uninstall ingress-nginx --namespace kube-system
else
    echo "Please provide arguments: 'create' or 'delete'"
fi

#curl https://gitlab.com/nktsingh22/init-scripts/-/raw/main/nginx-ingress-controller.sh | bash -s -- create
#curl https://gitlab.com/nktsingh22/init-scripts/-/raw/main/nginx-ingress-controller.sh | bash -s -- delete